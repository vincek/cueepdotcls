% class_cueep_vl
% Copyright 2011-2020 V. Ledda
% Contact: v.ledda@laposte.net

%  This file is part of class_cueep_vl.

%     class_cueep_vl is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation version 2.

%     class_cueep_vl is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.

%     You should have received a copy of the GNU General Public License
%     along with class_cueep_vl.  If not, see <http://www.gnu.org/licenses/>.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%     Identification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{cueep}[2020/14/09 Une classe pour les fiches du cueep de V. Ledda, v0.85]



\LoadClassWithOptions{extarticle}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Preliminary declarations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{eurosym,vmargin,multicol,keyval}
\RequirePackage{ifpdf}
\RequirePackage[french]{babel}
\RequirePackage{graphicx,fancybox}

\RequirePackage{amsthm,mathtools}
\RequirePackage[table,x11names]{xcolor}
%\RequirePackage{url}


\ifpdf
\RequirePackage{xr-hyper}
\RequirePackage[pdftex,ocgcolorlinks,linkcolor=SteelBlue4]{hyperref}
\DeclareGraphicsRule{*}{mps}{*}{}
\else
\RequirePackage{hyperref}
\fi





\RequirePackage{answers}

\RequirePackage[autostyle]{csquotes}
\RequirePackage[babel]{microtype}
\RequirePackage{enumerate,pifont,nccrules}
\RequirePackage{picins,calc}
\RequirePackage[light,frenchstyle,narrowiints,partialup]{kpfonts}
\RequirePackage{fancyhdr}
\RequirePackage{ifthenx}

\RequirePackage{ocgx}


\RequirePackage{tikz}
\usetikzlibrary{calc,arrows,shadings,shadows}

\RequirePackage[framemethod=tikz]{mdframed}


\newcounter{susis}

\newcommand{\myHyperlink}[2]{%
\refstepcounter{susis}\label{susislink#1}%
\ifcsname r@susistarget#1\endcsname
\hyperlink{#1}{\textcolor{SteelBlue4}{#2}}%
\else

\fi
}


\newcommand{\myHypertarget}[2]{%
\refstepcounter{susis}\label{susistarget#1}%
\ifcsname r@susislink#1\endcsname
\hypertarget{#1}{#2}%
\else
\hypertarget{#1}{\fbox{\textcolor{red}{#2}}}%
\fi
}


%%%%%%%%%%%%%%%%%%%%%%%%
%Dimensions et compteurs
%%%%%%%%%%%%%%%%%%%%%%%%


\newcount\bibli
\newcount\print

\bibli=0
\print=0
\newcounter{currentExo}
\newcounter{currentExemple}
\newcount\nosolout%
\newcount\solnosecnum%
\solnosecnum=0%
%formats

\newdimen\largeur
\newcounter{Itemm}
\parindent=0mm

\newcount\logoE
\logoE=0
\newcount\solution
\newcount\cours
\cours=0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Options section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareOption{print}{%
  \print=1
}

\DeclareOption{bibli}{%
\bibli=1
 }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solutions des exercices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareOption{sol}{%
\solution=1
\nosolout=0
\Newassociation{sol}{Solution}{ans}

\newcommand{\titrePageSolution}{\begin{center}\textbf{Éléments de réponses aux exercices}\end{center}}
\newcommand{\solTexte}{Les éléments de correction n'ont pas pour
  objectif de donner un corrigé type. Parfois la rédaction est volontairement lacunaire, c'est au lecteur de combler les manques pour parfaire sa
  compréhension des sujets abordés.}

\newcommand{\SolTexte}{\textit{\solTexte}\vspace{10mm}\\}

\newcommand{\soltexte}[1]{\renewcommand{\solTexte}{#1}}


\renewenvironment{Solution}[1]{\setcounter{currentExo}{#1}\hypertarget{sol:#1}{Correction} de l'\hyperlink{exo:\thecurrentExo}{\bf exercice #1}\par}{%
  \ifthenelse{\print=0}{%
    \begin{flushright}
      {\scriptsize     \hyperlink{exo:\thecurrentExo}{Énoncé de l'exercice n°\thecurrentExo.}}
    \end{flushright}
  }{}
}
}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% options pdf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newcommand{\Author}{V. Ledda}
\newcommand{\Producer}{Centrale Lille}
\newcommand{\TItle}{}
\newcommand{\Keywords}{}
\newcommand{\Composante}{Centrale Lille}

%###############################################
%### Gestion H and F
%###############################################



\fancyhead{}
\fancyfoot{}


\pagestyle{fancy}


\fancyfoot[C]{\today}
\fancyfoot[R]{\Author}
\fancyfoot[L]{\SFC}



\newcommand{\composante}[1]{\renewcommand{\Composante}{#1}}
\newcommand{\ladate}[1]{\fancyfoot[C]{#1}}
\newcommand{\auteur}[1]{\renewcommand{\Author}{#1}\fancyfoot[R]{#1}\author{#1}}
\newcommand{\organisme}[1]{\renewcommand{\Producer}{#1}\fancyfoot[L]{#1}}
%\newcommand{\plogo}[1]{\fancyhead[L]{#1}}
\newcommand{\titre}[1]{\renewcommand{\TItle}{#1}\fancyhead[C]{\large\bf   #1}\title{\sc #1}}
\newcommand{\reference}[1]{\fancyhead[R]{#1}}
\newcommand{\motsclefs}[1]{\renewcommand{\Keywords}{#1}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Format %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\DeclareOption{A4}{%
\setpapersize{A4}
\setmargins{10mm}{10mm}{190mm}{252mm}{15mm}{5mm}{0mm}{10mm}
\setlength\largeur{\textwidth}
\headwidth=190mm


\renewcommand{\footrulewidth}{0pt}
\renewcommand{\headrulewidth}{0.4pt}
}

\DeclareOption{A4R}{%
\setpapersize[landscape]{A4}
\setmargins{10mm}{10mm}{277mm}{190mm}{0mm}{0mm}{0mm}{0mm}
%\setlength\largeur{127.5mm}
\pagestyle{empty}
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Cours     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\DeclareOption{Cours}{%
\cours=1
\setpapersize{A4}
\setmarginsrb{15mm}{15mm}{15mm}{10mm}{10mm}{7mm}{10mm}{15mm}
\setlength\largeur{\textwidth}
\setlength{\headwidth}{\textwidth}
\ladate{\thepage}
\solnosecnum=1
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\headrulewidth}{0.4pt}
\fancypagestyle{plain}
{
  \fancyhf{}\renewcommand{\headrulewidth}{0pt}
  \fancyhead[R]{\Composante}
}
\Newassociation{deve}{devexemple}{ans1}


\renewenvironment{devexemple}[1]{\setcounter{currentExemple}{#1}\myHypertarget{dexem:#1}{Développement} de l'\hyperlink{exem:\thecurrentExemple}{\bf exemple #1}\par}{%

   \begin{flushright}
      {\scriptsize     \hyperlink{exem:\thecurrentExemple}{Exemple n°\thecurrentExemple.}}
    \end{flushright}

}
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Logo      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareOption{logo}{%
\newdimen\hlogo
\setlength{\hlogo}{10mm}
\logoE= 1
\newcommand{\Logo}{logo}
\newcommand{\logo}[1]{\renewcommand{\Logo}{#1}}

\newcommand{\logoh}[1]{\setlength{\hlogo}{#1}}
%\fancyhead[L]{\includegraphics[height=\hlogo]{\Logo}}



%\newcommand{\logoh}[2]{\renewcommand{\Logo}{#1}\setlength\hlogo{#2}}%
% }

%

%\showthe\logoE
%\ifthenelse{\equal{\logoE}{1}}{\showthe\logoE\fancyhead[L]{\showthe\logoE ghjk}}

%\show\logoE%
%\ifthenelse{\lengthtest{\hlogo = 0cm}}{}{idfghj\setmargins{10mm}{10mm}{140mm}{267mm-\hlogo}{\hlogo}{4mm}{0mm}{10mm}\fancyhead[L]{\includegraphics[height=\hlogo]{\Logo}}}
%}

}





%taille de la police








\DeclareOption{11pt}{\fontsize{11}{12}\selectfont}
\DeclareOption{12pt}{\fontsize{12}{14.5}\selectfont}
\DeclareOption{10pt}{\fontsize{10}{13.6}\selectfont}
\DeclareOption{8pt}{\fontsize{4}{10.7}\selectfont}



\ExecuteOptions{A4,10pt}
\ProcessOptions\relax





%%%%%%%%%%%%%%%%%%%%%%%%
%% macro pour la saisie
%%%%%%%%%%%%%%%%%%%%%%%

\input{macro_cueep.tex}

\newcommand{\Cueep}{\large\fontfamily{cyklop}\selectfont C{\scshape
  u\hspace{-0.5mm}\scalebox{2}[1]{e}\hspace{-1.3mm}\scalebox{2}[1]{e}\hspace{-0.5mm}p}\fontfamily{familydefault}\selectfont}

\newcommand{\SFC}{\includegraphics[height=8mm]{logo_iteem}}
%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% qcm
%%%%%%%%%%%%%%%%%%%%%%%
\newcount\qcmalpha
\newcommand{\QcmTexte}{{\bf Choississez parmi les réponses proposées l'unique
  bonne réponse à chaque question. Chaque réponse exacte rapporte 1 point. Une
  réponse inexacte enlève 0,25 point. Une question sans réponse ne
  rapporte ni n'enlève aucun point. Si le total est négatif, il est
  ramené à zéro.}
}

\newcommand{\qcmTexte}[1]{\renewcommand{\QcmTexte}{#1}}

\qcmalpha= 0
%\showthe\qcmalpha
\newcommand{\qcmAlpha}[0]{\qcmalpha=1}
\newcommand{\qcmBoite}[0]{\qcmalpha=0}

\newenvironment{Qcm}[1][2]
{
\newenvironment{qcm}[1][#1]
{
\setlength\columnsep{4mm}
\setlength\columnseprule{0pt}
  \begin{multicols}{#1}
    \ifthenelse{\qcmalpha=
      0}{\begin{Pilist}{pzd}{114}}{\begin{enumerate}[\bf A.]}}
    {\ifthenelse{\qcmalpha= 0}{\end{Pilist}}{\end{enumerate}}\end{multicols}
}
\QcmTexte


\begin{enumerate}
}{
\end{enumerate}

}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nouvelles commandes graphiques
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Figures habillées.

\newlength\figh
\newlength\figw
\newlength\coeff
\def\echell{1}
\def\format{eps}
\def\position{l}
%decoration permet de passer les pramètres d'embellissement Ã  parpic.
\def\decoration{}
\def\style{\arabic}
\newsavebox{\Figureb}
\newdimen\total
\newdimen\reste
%\showthe\largeur
%\showthe\baselineskip
%\showthe\baselinestretch
\newdimen\ligneskip
\setlength\ligneskip{\baselinestretch\baselineskip}
%\showthe\ligneskip
\newenvironment{Figures}[2]{%
  \setkeys{param}{#1}\setcounter{Itemm}{1}

\setlength{\itemindent}{0pt}
\renewcommand{\item}{%
  \ifthenelse{\equal{\theItemm}{1}}{\quad\style{Itemm}.\;\;\stepcounter{Itemm}}{\hfill\break$\mbox{}${\quad\style{Itemm}.\;\;\stepcounter{Itemm}}}}
% la boite sert à trouver la hauteur
\par\begin{lrbox}{\Figureb}

  \begin{minipage}[h]{\largeur}

    \parpic(\figw,\figh)[\position\decoration]{\parbox[b]{\figw}{\placeimage{\format}{#2}}}}{%
\renewcommand{\item}{%
        \@inmatherr\item  \@ifnextchar[\@item{\@noitemargtrue%
          \@item[\@itemlabel]}}
\end{minipage}
\end{lrbox}

\usebox{\Figureb}
\setlength\total{\ht\Figureb+\dp\Figureb}
\setlength\reste{\figh-\total}
%\showthe\figh
%\showthe\total
%\showthe\reste
\ifthenelse{\lengthtest{\reste >
    0pt}}{\vspace{\reste}\vspace{\ligneskip}\par}{\ifthenelse{\lengthtest{\reste>
      -5\ligneskip  }}{\vspace{\ligneskip}\par}{\par}}

}

\define@key{param}{position}{\def\position{#1}}
\define@key{param}{enumstyle}{\def\enumstyle{#1}}
\define@key{param}{decoration}{\def\decoration{#1}}
\define@key{param}{hauteur}{\setlength{\figh}{#1}}
\define@key{param}{format}{\def\format{#1}}
\define@key{param}{enumstyle}{\ifthenelse{\equal{#1}{r}}{\def\style{\roman}}{\ifthenelse{\equal{#1}{R}}{\def\style{\Roman}}{\ifthenelse{\equal{#1}{a}}{\def\style{\arabic}}{\ifthenelse{\equal{#1}{al}}{\def\style{\alph}}{\ifthenelse{\equal{#1}{Al}}{\def\style{\Alph}}{}}}}}}
\define@key{param}{largeur}{\setlength{\figw}{#1}}
\define@key{param}{echelle}[1]{\def\echell{#1}}

%\setlength\coeff{\figw * \echell}
%\showthe\coeff
\newcommand{\placeimage}[2]{\ifthenelse{\equal{#1}{pstex}}{\resizebox{110mm}{!}{\input{#2}}}{\ifthenelse{\equal{#1}{eps}}{\includegraphics[
      scale=\echell, keepaspectratio=true,width=\figw,height=\figh]{#2}}{\scalebox{\echell}{#2}}}}


%%%%%%%%%%%%%%%%%%%%%
% style
%%%%%%%%%%%%%%%%%%%%%

%Rien pour l'instant

 \input{style_cueep_base.tex}

%%%%%%%%%%%%%%%
%Environnements
%%%%%%%%%%%%%%%

% exercices
\newcounter{Exercices}
\setcounter{Exercices}{0}

\newenvironment{exercice}[1][0]{\refstepcounter{Exercices}\noindent\titreExercice[\theExercices]\ifthenelse{\equal{#1}{0}}{}{\points{#1}}\\}{\par\vspace{2mm}}

\ifthenelse{\solution=1}{%
  \renewenvironment{exercice}[1][0]{%
    \refstepcounter{Exercices}%
    \noindent\hypertarget{exo:\theExercices}{}\hyperlink{sol:\theExercices}
    {\titreExercice[\theExercices]}\ifthenelse{\equal{#1}{0}}{}{\textbf{#1}\par{\vspace{0.5mm}}}}{\par\vspace{2mm}}}



\newenvironment{exercice*}[1][0]{\noindent{\titreExercice}\ifthenelse{\equal{#1}{0}}{}{\points{#1}}}{\par\vspace{2mm}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Début&fin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\AtBeginDocument{%
  \ifthenelse{\solution =1}{\Opensolutionfile{ans}[\jobname_ans]}{}%
  \ifthenelse{\cours =1}{\Opensolutionfile{ans1}[\jobname_exemples]}{}%
\hypersetup{%
pdftitle={\TItle},%
pdfauthor={\Author},%
pdfkeywords={\Keywords},%
pdfproducer={\Producer}%
}%
}



\AtEndDocument{%
  \ifthenelse{\solution=1}{%
    \Closesolutionfile{ans}%
    \ifthenelse{\nosolout=0}{%
      \newpage%
      \ifthenelse{\solnosecnum=0}{\section{Éléments de correction}}{\section*{Éléments de    correction}}%
      \label{sec:cor}%
      \SolTexte%
      \Readsolutionfile{ans}%
    }%
    {}}{}%
  \ifthenelse{\cours=1}{%
    \Closesolutionfile{ans1}\ifthenelse{\etudiant=0}{%
      \newpage%
       \section{Développement des exemples}\label{sec:exemples}%
     \Readsolutionfile{ans1}%
   }{}%
 }{}%
 }
